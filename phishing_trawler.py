#Copyright 2017 Andrew Zuelsdorf. All rights reserved.

import sys
import imaplib
import getpass
import re
import email.header
import IPy
import os
import pickle
import pygeoip
import socket
import argparse
import checksum_util

class Analyzer(object):

    #This creates the Analyzer object and logs in to the IMAP server for us.
    #username = the username of the account on the server.
    #password = the password for the account on the server.
    #server = the name of the server.
    #trust_file_name = The file where we saved or will save our trust map.
    #update = Whether we want to update our trust map with whatever new emails
    #are in our email account.
    #verify_last = The number of most-recent emails we still need to verify.
    #verify_level = How strict our comparisons of email metadata should be in
    #order to believe that an email is trustworthy.
    def __init__(self, username, password, server="imap.gmail.com",
            trust_file_name=None, update=False, mailbox_name="INBOX",
            verify_last=0, database_file_name="GeoLiteCity.dat",
            verify_level="IP"):
        if trust_file_name is None:
            self.trust_file_name = None #Set cache_file_name to None if it is
            #not given.
        else:
            self.trust_file_name = "{0}".format(trust_file_name) #Store the
            #name of the cache file since we will use it to store our updated
            #cache as well.

        #Open up the geolocation database.
        self.database = pygeoip.GeoIP(database_file_name)

        #Store credentials, server name, mailbox name, verification level
        self.password = "{0}".format(password)
        self.username = "{0}".format(username)
        self.server = "{0}".format(server)
        self.mailbox_name = "{0}".format(mailbox_name)
        self.verify_level = "{0}".format(verify_level)

        #Load the cache if it exists. If it doesn't exist, create an empty dictionary.
        if self.trust_file_name is not None and os.path.exists(self.trust_file_name):
            trust_file = open(self.trust_file_name, 'rb')
            self.trust_map = pickle.load(trust_file)
            trust_file.close()
        else:
            self.trust_map = dict()

        #Create a connection to imap server.
        self.login()

        if update:
            print("Updating trust list. This could take a few minutes. Please wait.")
            self.get_trust_map(mailbox_name, verify_last)
            print("Finished updating trust list.")

    def get_sender_IP_address(self, msg):
        sender_ip = None
       
        #Get all "Received" headers in the email.
        received_headers = msg.get_all(name="Received", failobj=[])
  
        #If there are any "Received" headers...
        if len(received_headers) > 0:
            #We care about the chronologically first "from" IPv4
            #address in the "Received" headers.

            #Go through the "Received" headers in reverse order
            #(the older Received headers will be at the end of
            #the list, and we care about the oldest header with
            #a "from" public IPv4 address or a "from" public
            #IPv6 address). 
            for header in reversed(received_headers):
                decoded_header = email.header.make_header(
                        email.header.decode_header(header))
                string_header = str(decoded_header)

                #Check for public IPv4 addresses...
                matches = re.findall(
                        r"[^0-9\.]+([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})[^0-9\.]+",
                        string_header)

                #Check for public IPv6 addresses...
                matches.extend(re.findall(
                        r"\[([0-9a-f\:]+)\]",
                        string_header))

                #Find first public IP address and return it. If none
                #exists in this header, then move on to the
                #next-least-recent header.
                if string_header.startswith("from") and matches != []:
                    for match in matches:
                        try:
                            #TODO: Is LINKLOCAL in IPv6 the same as private
                            #IPv4 address? If so, then shouldn't accept it.
                            ip_type = IPy.IP(match).iptype() 
                            if ip_type == "PUBLIC":
                                sender_ip = "{0}".format(match)
                                break
                            else:
                                print("IP address \"{0}\" has type \"{1}\"".format(match, IPy.IP(match).iptype()))
                        except ValueError:
                            pass
                    if sender_ip is not None:
                        break
                else:
                    print("Matches: {0}".format(matches))
                    print("String header: {0}".format(string_header))

        return sender_ip

    def get_sender_email_address(self, msg):
        sender_email = None

        #Get the email address from which this email was sent.
        from_headers = msg.get_all(name="From", failobj=[])
        
        if len(from_headers) > 0:
            for header in from_headers:
                 decoded_header = email.header.make_header(
                         email.header.decode_header(header))
                 sender_email = u"{0}".format(decoded_header)
                 break

        return sender_email

    def get_sender_email_body(self, msg):
        body = list()

        if msg.is_multipart():
            for part in msg.walk():
                content_type = part.get_content_type()
                content_disposition = part.get("Content-Disposition")

                if (content_disposition is None or 'attachment' not in content_disposition) and content_type == "text/html":
                    body.append(part.get_payload(decode=True))
        else:
            body.append(msg.get_payload(decode=True))

        return body

    def get_sender_email_subject(self, msg):
        subject = None

        #Get the email address from which this email was sent.
        subject_headers = msg.get_all(name="Subject", failobj=[])
        
        if len(subject_headers) > 0:
            for header in subject_headers:
                 decoded_header = email.header.make_header(
                         email.header.decode_header(header))
                 subject = u"{0}".format(decoded_header)
                 break

        return subject

    def store_email_metadata(self, sender_ip, sender_email, sender_checksum):
        #If we have a sender email address and a sender IP address,
        #then add them to our map.
        if sender_email is not None and sender_ip is not None:
            if sender_email not in self.trust_map:
                self.trust_map[sender_email] = dict()
                self.trust_map[sender_email]['IP'] = set()
                self.trust_map[sender_email]['City'] = set()
                self.trust_map[sender_email]['Region'] = set()
                self.trust_map[sender_email]['Country'] = set()
                self.trust_map[sender_email]['Checksum'] = set()
            try:
                match = self.database.record_by_name(sender_ip)
                location = "{0}, {1}, {2}".format(match['city'],
                        match['region_code'], match['country_code'])
                self.trust_map[sender_email]['IP'].add(sender_ip)
                self.trust_map[sender_email]['City'].add("{0}, {1}, {2}".format(match['city'], match['region_code'], match['country_code']))
                self.trust_map[sender_email]['Region'].add("{0}, {1}".format(match['region_code'], match['country_code']))
                self.trust_map[sender_email]['Country'].add(match['country_code'])
                self.trust_map[sender_email]['Checksum'].add(sender_checksum)
            except socket.gaierror:
                self.trust_map[sender_email]['IP'].add(sender_ip)
                self.trust_map[sender_email]['Checksum'].add(sender_checksum)

    def get_trust_map_aux(self, mailbox_name, verify_length=0):
        email_to_ip = dict()

        #Indicate that we want to run queries on the mailbox with the name
        #mailbox_name.
        return_code, data = self.imap_connection.select(mailbox_name)

        #This is a trivial search: it returns the id numbers of all
        #emails in this mailbox, regardless of sender, subject, or
        #other attributes. The "data" is a list containing as its
        #first element a space-separated string of id numbers
        #indicating which email fit out search criteria.
        return_code, data = self.imap_connection.search(None, "ALL")

        #Check to see that the operation succeeded.
        if return_code.upper() == "OK":
            #Iterate through all the numbers in the string except the last
            #verify_length emails.
            email_ids = re.split(r"\s+", str(data[0], "utf-8"))

            for id_number in email_ids[0:(len(email_ids) - verify_length):1]:
                #Get the data for the email that has the id number id_number.
                #The data will be returned in a format specified by RFC822.
                #This way, we know how the data is stored so we can parse it
                #and make sense of it. 
                return_code, data = self.imap_connection.fetch(id_number, "(RFC822)")

                #Check to see that the operation succeeded.
                if return_code.upper() == "OK":
                    try:
                        msg = email.message_from_bytes(data[0][1]) #Put the
                        #bytes into an email object whose headers we need.
  
                        #Get metadata on this email.
                        sender_ip = self.get_sender_IP_address(msg)
                        sender_email = self.get_sender_email_address(msg)
                        msg_checksum = checksum_util.get_sha_512_checksum_list(
                                self.get_sender_email_body(msg))
        
                        #Use sender IP address to figure out location, then 
                        #add that information to our map of email addresses
                        #to location information.
                        self.store_email_metadata(sender_ip, sender_email, msg_checksum)
                        #print("Email address {0}; IP address {1}; Location {2}".format(
                        #    sender_email, sender_ip, location)) 
                    except UnicodeEncodeError as uee:
                        print("Could not decode email with id \"{0}\"".format(
                            id_number))
                        raise uee
                else:
                    print("Could not fetch email with id \"{0}\"".format(id_number))
    
    def get_trust_map(self, mailbox_name="INBOX", verify_last=0):
        #List all mailboxes on this server
        return_code, folders = self.imap_connection.list()

        #Check that the operation succeeded
        if return_code.upper() == "OK":
            #If the mailbox we care about is among the mailboxes on this server...
            if any([(mailbox_name in str(folder, "utf-8")) for folder in folders]):
                #Get a map of email addresses to trusted IPv4 addresses.
                self.get_trust_map_aux(mailbox_name, verify_length=verify_last)

    def __del__(self):
        if self.imap_connection is not None:
            self.imap_connection.logout()
        if self.trust_file_name is not None:
            trust_file = open(self.trust_file_name, 'wb')
            pickle.dump(self.trust_map, trust_file)
            trust_file.close()

    def login(self):
        #Create a connection to imap server.
        self.imap_connection = imaplib.IMAP4_SSL(self.server)

        try:
            #Log in to the server. The "data" is just a message saying whether the login
            #succeeded or not.
            return_code, data = self.imap_connection.login(self.username, self.password)

            #Check that the operation succeeded.
            if return_code.upper() != "OK":
                print("Login failed with return code \"{0}\"".format(return_code))
                sys.exit(-1)

        #Catch likely exceptions
        except imaplib.IMAP4.error as err:
            #Set connection to None since we never logged in and the destructor
            #will try to log us out unless we set the connection to None.
            self.imap_connection = None
            #Let the caller handle it since there's no reasonable and
            #intuitive way to handle this here.
            raise err

    def inspect(self, verify_last):
        #Check that we are logged in.
        if self.imap_connection is None:
            self.login()

        #Indicate that we want to run queries on the mailbox with the name
        #mailbox_name.
        return_code, data = self.imap_connection.select(self.mailbox_name)

        #This is a trivial search: it returns the id numbers of all
        #emails in this mailbox, regardless of sender, subject, or
        #other attributes. The "data" is a list containing as its
        #first element a space-separated string of id numbers
        #indicating which email fit out search criteria.
        return_code, data = self.imap_connection.search(None, "ALL")

        verified_emails = list()
        unverified_emails = list()
        new_emails = list()

        #Check to see that the operation succeeded.
        if return_code.upper() == "OK":
            #Iterate through all the numbers in the string except the last
            #verify_length emails.
            email_ids = re.split(r"\s+", str(data[0], "utf-8"))
  
            if len(email_ids) - verify_last <= 0:
                raise ValueError("You asked to verify the last {0} email(s) in your inbox but you only have {1} email(s) in your mailbox. Please verify a number of emails less than the total number of emails in your mailbox.\n".format(verify_last, len(email_ids)))
                sys.exit(-1)

            for id_number in email_ids[(len(email_ids) - verify_last):]:
                #Get the data for the email that has the id number id_number.
                #The data will be returned in a format specified by RFC822.
                #This way, we know how the data is stored so we can parse it
                #and make sense of it. 
                return_code, data = self.imap_connection.fetch(id_number, "(RFC822)")

                #Check to see that the operation succeeded.
                if return_code.upper() == "OK":
                    try:
                        msg = email.message_from_bytes(data[0][1]) #Put the
                        #bytes into an email object whose headers we need.
  
                        sender_ip = self.get_sender_IP_address(msg)
                        sender_email = self.get_sender_email_address(msg) 
        
                        #If we have a sender email address and a sender IP address,
                        #then add them to our map.
                        if sender_email is None:
                            print("Could not find sender email address in email with id \"{0}\"".format(
                                id_number))
                            #print(msg)
                            unverified_emails.append(msg)
                        elif sender_ip is None:
                            print("Could not find sender IP address in email with id \"{0}\"".format(id_number))
                            #print(msg)
                            unverified_emails.append(msg)
                        else:
                            print("---------------------------")
                            if sender_email in self.trust_map:
                                try:
                                    match = self.database.record_by_name(sender_ip)
                                except socket.gaierror:
                                    match = None

                                if self.compare(sender_ip, match,
                                        self.trust_map[sender_email]):
                                    print("Email from \"{0}\" is valid.".format(
                                        sender_email))
                                    verified_emails.append(msg)
                                else:
                                    print("Email from \"{0}\" is not valid.".format(
                                        sender_email))
                                    unverified_emails.append(msg)
                            else:
                                print("Have received email from new sender \"{0}\".".format(
                                    sender_email))
                                new_emails.append(msg)
                            print("---------------------------")
                    except UnicodeEncodeError as uee:
                        print("Could not inspect email with id \"{0}\"".format(id_number))

                        raise uee

        return verified_emails, unverified_emails, new_emails

    def compare(self, sender_ip, match, verified_info):
        if self.verify_level == "IP":
            return sender_ip in verified_info["IP"]
        elif match is not None:
            if self.verify_level == "CITY" and 'City' in verified_info:
                return "{0}, {1}, {2}".format(match['city'], match['region_code'], match['country_code']) in verified_info['City']
            elif self.verify_level == "REGION" and "Region" in verified_info:
                return "{0}, {1}".format(match['region_code'], match['country_code']) in verified_info['Region']
            elif self.verify_level == "COUNTRY" and 'Country' in verified_info:
                return match['country_code'] in verified_info['Country']
            else:
                raise ValueError(
                        "\"{0}\" is not a valid verification level.".format(
                            self.verify_level))
        else:
            return False

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('-user', '--username', dest="username", type=str,
            required=True, help='Email address to use for login to IMAP server.')
    parser.add_argument('-v', '--verify-last', dest="verify_last", type=int,
            required=False, default=10,
            help='Number of most-recent emails we should verify based on the less-recent emails in the user\'s inbox. Default is 10.')
    parser.add_argument('-i', '--imap-server', dest="imap_server", type=str,
            required=False, default='imap.gmail.com',
            help='Number of most recent emails whose origins we should verify based on the less-recent emails in the user\'s inbox. Default is Gmail\'s imap server.')
    parser.add_argument('-m', '--mailbox-name', dest="mailbox_name", type=str,
            required=False, default='INBOX',
            help='Name of mailbox to use.')
    parser.add_argument('-u', '--update', dest="update", action="store_true",
            required=False, default=False,
            help='Update information on where your correspondents usually send their email from. It is a good idea to do this if you know your correspondents have traveled recently.')
    parser.add_argument('-l', '--verify-level', dest="verify_level", type=str,
            required=False, default="REGION",
            help='How strict our verification should be. Levels are \"IP\" (email is trustworthy if sender has used this IP address before), \"CITY\" (email is trustworthy if sender has sent email from this city before), \"REGION\" (email is trustworthy if sender has sent email from this region, such as state or province, before), and \"COUNTRY\" (email is valid if sender has sent email from this country before).')
    parser.add_argument('-t', '--trust-file', dest="trust_file_name", type=str,
            required=False, default="trust_file.bin",
            help="The name of the file where information on your correspondents\' emails usually come from. This file doesn't have to exist already.")

    args = parser.parse_args()

    #Input santization
    if not (args.verify_level == "IP" or args.verify_level == "CITY" or
            args.verify_level == "REGION" or args.verify_level == "COUNTRY"):
        sys.stderr.write("You set verification level to \"{0}\". Please set verification level to either \"IP\", \"CITY\", \"REGION\", or \"COUNTRY\"\n".format(args.verify_level))
        sys.exit(-1)

    if args.verify_last <= 0:
        sys.stderr.write("You asked to verify the last \"{0}\" email(s) in your inbox. Please ask to verify at least one email in your inbox.\n".format(args.verify_last))

    #Enter the same password twice because it is more difficult to enter
    #the exact same wrong password twice than it is to enter a wrong
    #password once. This password will be used to log in to the server.
    password = 'a'
    confirmed_password = 'b'
    while password != confirmed_password:
        password = "mcix flfl lpgl aiwm"
        confirmed_password = "mcix flfl lpgl aiwm"
        #password = getpass.getpass("Please enter your password (for your security, the password will not appear as you type it): ")
        #confirmed_password = getpass.getpass("Please re-enter your password: ")
        if password != confirmed_password:
            print("Passwords do not match. Please try again.")

    verify_last = args.verify_last
    verify_level = args.verify_level
    username = args.username
    server = args.imap_server
    mailbox_name = args.mailbox_name
    trust_file_name = args.trust_file_name
    update = args.update
    database_file_name = os.path.join("GeoLiteCity", "GeoLiteCity.dat")

    analyzer = Analyzer(username, password, server, trust_file_name, update,
            mailbox_name, verify_last, database_file_name, verify_level)

    verified, unverified, new = analyzer.inspect(verify_last)

    email_number = 1
    for email in verified:
        email_file = open(os.path.join("older", "verified_email_{0}.html".format(email_number)),
                'w', encoding="utf-8")
        email_file.write(analyzer.get_sender_email_body(email))
        email_number += 1
        email_file.close()

    email_number = 1
    for email in unverified:
        email_file = open(os.path.join("older", "unverified_email_{0}.html".format(email_number)),
                'w', encoding="utf-8")
        email_file.write(analyzer.get_sender_email_body(email))
        email_number += 1
        email_file.close()
        
    email_number = 1
    for email in new:
        email_file = open(os.path.join("older", "new_email_{0}.html".format(email_number)),
                'w', encoding="utf-8")
        email_file.write(analyzer.get_sender_email_body(email))
        email_number += 1
        email_file.close()

if __name__ == "__main__":
    main()
