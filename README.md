# README #

### What is this repository for? ###

This repository is for people who want to check for forged emails.

### How do I get set up? ###

Installation:

sudo pip install -r requirements.txt

### Contribution guidelines ###

Coming soon.

### Who do I talk to? ###

Email azuelsdorf16@gmail.com if you have any questions or concerns.

### Miscellaneous ###

This software is licensed under the GNU LGPLv3.