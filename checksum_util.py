import hashlib

def get_sha_512_checksum(binary_string):
    return hashlib.sha512(binary_string).hexdigest()

def get_sha_512_checksum_list(binary_string_list):
    m = hashlib.sha512()
    for binary_string in binary_string_list:
        m.update(binary_string)
    return m.hexdigest()

def main():
    string1 = b"I'm slim shady, yes I'm the real shady\n"
    string2 = b"all you other slim shadies are just imitating.\n"
    string3 = b"I'm slim shady, yes I'm the real shady\nall you other slim shadies are just imitating.\n"
    string_list = [string1, string2]

    assert(get_sha_512_checksum(string1) != get_sha_512_checksum(string2))

    with open("string1.bin", 'wb') as file1:
        file1.write(string1)

    with open('string1.bin', 'rb') as file1:
        temp_string = file1.read()

    assert(get_sha_512_checksum(temp_string) == get_sha_512_checksum(string1))

    assert(get_sha_512_checksum(string3) == get_sha_512_checksum_list(string_list))

if __name__ == "__main__":
    main()
