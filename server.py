import flask
import os
import base64
import phishing_trawler

app = flask.Flask(__name__, static_url_path="")
app.secret_key = base64.b64encode(os.urandom(64)).decode('utf-8')

#If someone forgets the /calendar.html on the end of
#a URL, then just send them to the calendar.
@app.route("/", methods=["GET"])
def serve_homepage():
    return app.send_static_file("homepage.html")

def save_emails_to_file(analyzer, emails, username, email_stem, email_path='email_accounts'):
    email_file_paths = list()

    email_directory = os.path.join(email_path, username)

    if not os.path.exists(email_directory):
        os.makedirs(email_directory)

    email_number = 1
    for email in emails:
        email_file_path = os.path.join(
            email_directory,
            '{0}{1}.html'.format(email_stem, email_number))
        email_file_paths.append(email_file_path)
        email_file = open(email_file_path, 'wb')
        for part in analyzer.get_sender_email_body(email):
            email_file.write(part)
        email_number += 1
        email_file.close()

    return email_file_paths

class Metadata(object):
    def __init__(self, sender_email_address, email_subject, email_location):
        if sender_email_address is not None:
            self.sender = "{0}".format(sender_email_address)
        else:
            self.sender = "N/A"
        if email_subject is not None:
            self.subject = "{0}".format(email_subject)
        else:
            self.subject = "N/A"
        if email_location is not None:
            self.link = "{0}".format(email_location)
        else:
            self.link = "N/A"

@app.route("/email_accounts/<path:path>")
def serve_email(path):
    #TODO: Find a better way to do this.
    email_file = open(os.path.join("email_accounts", path), 'rb')
    to_return = email_file.read()
    email_file.close()
    return to_return

@app.route("/analyzer.html", methods=["POST"])
def analyze():
    keys = list()
    values = list()

    for key, value in flask.request.form.items():
        keys.append(key)
        values.append(value)

    form = flask.request.form

    username = form['username']
    verify_level = form['verify_level']
    verify_last = int(form['verify_last'])
    mailbox_name = form['mailbox_name']
    server = form['server']

    #TODO: Sanitize input.

    analyzer = phishing_trawler.Analyzer(username, 'mcix flfl lpgl aiwm', server, 'trust_file.bin', not os.path.exists("trust_file.bin"), mailbox_name, verify_last, os.path.join("GeoLiteCity", "GeoLiteCity.dat"), verify_level)

    verified_email, unverified_email, new_email = analyzer.inspect(verify_last)

    verified_paths = save_emails_to_file(analyzer, verified_email,
            username, 'verified_email_')
    unverified_paths = save_emails_to_file(analyzer, unverified_email,
            username, 'unverified_email_')
    new_paths = save_emails_to_file(analyzer, new_email, username,
            'new_email_')

    verified_addresses = [analyzer.get_sender_email_address(msg) for msg in verified_email]
    verified_subjects = [analyzer.get_sender_email_subject(msg) for msg in verified_email]
    verified_metadata = [Metadata(verified_addresses[i], verified_subjects[i], verified_paths[i]) for i in range(len(verified_addresses))]
    
    unverified_addresses = [analyzer.get_sender_email_address(msg) for msg in unverified_email]
    unverified_subjects = [analyzer.get_sender_email_subject(msg) for msg in unverified_email]
    unverified_metadata = [Metadata(unverified_addresses[i], unverified_subjects[i], unverified_paths[i]) for i in range(len(unverified_addresses))]
    
    new_addresses = [analyzer.get_sender_email_address(msg) for msg in new_email]
    new_subjects = [analyzer.get_sender_email_subject(msg) for msg in new_email]
    new_metadata = [Metadata(new_addresses[i], new_subjects[i], new_paths[i]) for i in range(len(new_addresses))]

    return flask.render_template("analyzer.html",
            keys=", ".join(keys), values=", ".join(values), verified_metadata=verified_metadata, unverified_metadata=unverified_metadata, new_metadata=new_metadata)

if __name__ == "__main__":
    app.run(debug=True, port=12345)
